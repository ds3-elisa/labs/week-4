package jala.sd.merchant.services;

import jala.sd.merchant.models.Product;
import jala.sd.merchant.repositories.ProductRepository;
import jala.sd.merchant.services.exceptions.ProductAlreadyExists;
import jala.sd.merchant.services.exceptions.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Product findById(UUID id) {
        Optional<Product> product = productRepository.findById(id);

        if(product.isEmpty()) {
            throw new ProductNotFoundException("Product Not Found.");
        }

        return product.get();
    }

    public void save(Product product) {
        boolean productExists = productRepository.findById(product.getId()).isPresent();

        if(productExists) {
            throw new ProductAlreadyExists("Product: " + product.getId() + ", Already Exists.");
        }

        productRepository.save(product);
    }
}
