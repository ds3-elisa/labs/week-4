package jala.sd.merchant.http.requestDTO;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

import java.util.List;
import java.util.UUID;

public record SaleRequestDTO(@NotNull List<UUID> productId, @NotNull @Min(1) int quantity) {}
